package com.example.espressotestproject;

import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import android.widget.EditText;

import androidx.test.espresso.Espresso;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import junit.framework.TestCase;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;


import java.util.Arrays;
import java.util.Collection;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest extends TestCase {

    private EditText etName;
    private EditText etTextToChange;

    @Rule
    public ActivityScenarioRule<MainActivity> ActivityTestRule= new ActivityScenarioRule<>(MainActivity.class);
    private String name="Kunal";
    private String message="Wassup";
    private String finalMessage=name+ " Says "+message.toUpperCase()+" WORLD! ";



    @Test
    public void testUserInputScenario()
    {
        //ActivityScenario scenario = ActivityTestRule.getScenario();

        //Input some text in the name field
        Espresso.onView(withId(R.id.etName)).perform(typeText(name));
        //Input some text in the edit text field
        Espresso.onView(withId(R.id.etMessageToBroadcast)).perform(typeText(message));
        //close soft keyboard
        Espresso.closeSoftKeyboard();
        //perform button Click
        Espresso.onView(withId(R.id.btnChange)).perform(click());
        //verify text
        Espresso.onView(withId(R.id.tvChangedText)).check(matches(withText(finalMessage)));

    }

    /*String getText(final Matcher<View> matcher) {
        final String[] stringHolder = { null };
        Espresso.onView(matcher).perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isAssignableFrom(TextView.class);
            }

            @Override
            public String getDescription() {
                return "getting text from a TextView";
            }

            @Override
            public void perform(UiController uiController, View view) {
                TextView tv = (TextView)view; //Save, because of check in getConstraints()
                stringHolder[0] = tv.getText().toString();
            }
        });
        return stringHolder[0];
    }*/


}