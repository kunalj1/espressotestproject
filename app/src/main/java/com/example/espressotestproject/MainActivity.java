package com.example.espressotestproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button btnChange;
    private EditText etName;
    private EditText etMessageToBroadcast;
    private TextView tvChangedText;
    private String finalMessage;
    private Editable message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnChange = (Button) findViewById(R.id.btnChange);

        etMessageToBroadcast = (EditText) findViewById(R.id.etMessageToBroadcast);

        etName = (EditText) findViewById(R.id.etName);

        tvChangedText= (TextView) findViewById(R.id.tvChangedText);

        btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                message=etMessageToBroadcast.getText();
                finalMessage=etName.getText() + " Says "+message.toString().toUpperCase()+" WORLD! ";
                tvChangedText.setText(finalMessage);
            }
        });
    }
}